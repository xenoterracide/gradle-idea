plugins {
    idea
    `java-library`
    `java-gradle-plugin`
    id("com.gradle.plugin-publish").version("0.10.0")
    id("com.xenoterracide.gradle.sem-ver").version("0.8.4")
}
group = "com.xenoterracide"
repositories {
    mavenCentral()
}

dependencyLocking {
    lockAllConfigurations()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

pluginBundle {
    // These settings are set for the whole plugin bundle
    vcsUrl = "https://bitbucket.org/xenoterracide/gradle-idea"
    website = vcsUrl
    plugins {
        create("idea-defaults") {
            id = "com.xenoterracide.gradle.$name"
            displayName = "Xeno's IntelliJ IDEA Configuration"
            description = displayName
            tags = listOf("idea")
        }
    }
}

